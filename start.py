import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello, World!'

@app.route('/my_route')
def my_route():
	return 'This is my route.'

if __name__ == "__main__":
	# setta la porta come la variabile d'ambiente PORT altrimenti, se la variabile è assente, a 5000
	port = int(os.environ.get("PORT", 5000))
	# setta analogamente l'host
	host = os.environ.get("HOST", "0.0.0.0")
	app.run(host = host, port = port)